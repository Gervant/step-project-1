const ourServicesDescription = document.querySelectorAll('.our__services__section-menu-descriptions');
const ourServicesTabs     = document.querySelectorAll('.our__services__section-menu-tab');

const ourAmazingWorkTabs = document.querySelectorAll('.our-amazing-work-menu-tab');
const ourAmazingWorkPictures = document.querySelectorAll('.our-amazing-work-gallery-picture');
const graphicDesignPictures = document.querySelectorAll('.graphic-design-picture');
const webDesignPictures = document.querySelectorAll('.web-design-picture');
const landingPagePictures = document.querySelectorAll('.landing-page-picture');
const wordpressPictures = document.querySelectorAll('.wordpress-picture');
const loadMoreBtn = document.querySelector('.load-more-btn');

const employeeDescMenuItem = document.querySelectorAll('.employee-menu-photos')
const employeeDescription = document.querySelectorAll('.employee-description')

const scrollLeft = document.querySelector('.left')
const scrollRight = document.querySelector('.right')

let activeItemIndex = 2;


ourServicesTabs.forEach(function (item){
    item.addEventListener('click', function (){
        let currentTab = item;
        let dataTab = currentTab.getAttribute('data-tab');
        let currentDescr = document.querySelector(dataTab);

        ourServicesTabs.forEach(function (item){
            item.classList.remove('our__services__section-menu-active-tab')
        })

        ourServicesDescription.forEach(function (item){
            item.classList.remove('active-descr')
        })

        currentTab.classList.add('our__services__section-menu-active-tab');
        currentDescr.classList.add('active-descr')
    })
})

ourAmazingWorkTabs.forEach(function (item) {

    item.addEventListener('click', function () {
        let currentTab = item;

        ourAmazingWorkPictures.forEach(function (elem) {
            elem.classList.remove('active')
        })

        if (currentTab.classList.contains('all-pictures-tab')) {
            ourAmazingWorkPictures.forEach(function (elem) {
                elem.classList.add('active')
            })
        }
        if (currentTab.classList.contains('graphic-design-tab')) {
            graphicDesignPictures.forEach(function (elem) {
                elem.classList.add('active')
            })
        }
        if (currentTab.classList.contains('web-design-tab')) {
            webDesignPictures.forEach(function (elem) {
                elem.classList.add('active')
            })
        }
        if (currentTab.classList.contains('landing-pages-tab')) {
            landingPagePictures.forEach(function (elem) {
                elem.classList.add('active')
            })
        }
        if (currentTab.classList.contains('wordpress-tab')) {
            wordpressPictures.forEach(function (elem) {
                elem.classList.add('active')
            })
        }

    })
})

loadMoreBtn.addEventListener('click', function () {
    let count = 0;
    for (let i = 0; i <= ourAmazingWorkPictures.length; i++) {
        if (!ourAmazingWorkPictures[i].classList.contains('active') && count <= 11) {
            ourAmazingWorkPictures[i].classList.add('active');
            count++;
        }
    }
})
employeeDescMenuItem.forEach(function (item) {
    item.addEventListener('click', function () {

        let dataTab = item.getAttribute('data-employee');
        let currentDescr = document.querySelector(dataTab);

        employeeDescription.forEach(function (item) {
            item.classList.remove('active-flex')
        })

        currentDescr.classList.add('active-flex')

    })
})
scrollLeft.addEventListener('click',function (){
    activeItemIndex--;

    for (let i = 0; i < employeeDescription.length; i++){
            employeeDescription[i].classList.remove('active-flex');
    }

    if (activeItemIndex === -1) {
        activeItemIndex = 3;
        employeeDescription[activeItemIndex].classList.add('active-flex');
    } else {
        employeeDescription[activeItemIndex].classList.add('active-flex');
    }

})
scrollRight.addEventListener('click',function (){
    activeItemIndex++;

    for (let i = 0; i < employeeDescription.length; i++){
        employeeDescription[i].classList.remove('active-flex');
    }

    if (activeItemIndex === 4) {
        activeItemIndex = 0;
        employeeDescription[activeItemIndex].classList.add('active-flex');
    } else {
        employeeDescription[activeItemIndex].classList.add('active-flex');
    }

})

